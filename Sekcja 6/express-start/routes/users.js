function userRoutes(app){
    app.post('/hello', (req, res) => {
        console.log(req.body);
        
        const {name, surname } = req.body;
      
        res.send(`Witaj ${name} ${surname}`)
      })
      
      app.get("/", (req, res) => {
      
        const {visitor_name} = req.cookies;
      
        if(visitor_name){
          res.send(`Witaj, ${visitor_name}`)
        }else{
          res.send('Czy my się znamy?');
        }
        console.log(req.cookies);
        res.send('Strona główna')
      });
      
      app.get("/hi/:name", (req, res) => {
        const {name} = req.params;
      
        const dt = new Date();
        dt.setDate(dt.getDate() +7)
        res.cookie('visitor_name', name, {
          // expires: dt
          maxAge: 5 * 60 * 1000
        });
      
        res.send('Imię zapisano ')
      });
      
      app.get('/logout', (req, res) => {
        res.clearCookie('visitor_name');
        res.redirect('/');
      })
}

module.exports = userRoutes;