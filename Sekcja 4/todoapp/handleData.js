const colors = require("colors");
const fs = require("fs");

const handleData = (type, title) => {
    const data = fs.readFileSync("dataDB.json");
    // const data = fs.readFileSync("dataDB.json", "utf8");
    // let data = fs.readFileSync('dataDB.json');
    // data = data.toString();
    let tasks = JSON.parse(data);
    //   console.log(tasks);
  
    if (type === 1 || type === 2) {
      const isExisted = tasks.find(task => task.title === title) ? true : false;
      if (type === 1 && isExisted) {
        return console.log("takie zadanie już istnieje".red);
      } else if (type === 2 && !isExisted) {
        return console.log("nie można usunąć zadania, które nie istnieje".red);
      }
    }
  
    let dataJSON = "";
    switch (type) {
      case 1:
        tasks = tasks.map((task, index) => ({
          id: index + 1,
          title: task.title
        }));
        console.log("dodaję zadanie");
        const id = tasks.length + 1;
        tasks.push({ id, title });
        dataJSON = JSON.stringify(tasks);
        //   console.log(dataJSON);
        fs.writeFileSync("dataDB.json", dataJSON);
        console.log(`dodaje zadanie: ${title}`.bgGreen);
        break;
      case 2:
        const index = tasks.findIndex(task => task.title === title);
        tasks.splice(index, 1);
        tasks = tasks.map((task, index) => ({
          id: index + 1,
          title: task.title
        }));
        dataJSON = JSON.stringify(tasks);
        //   console.log(tasks);
        fs.writeFile("dataDB.json", dataJSON, "utf8", err => {
          if (err) throw err;
          console.log(`Zadanie ${title} zostało usunięte`.bgGreen);
        });
        break;
      case 3:
        console.log(
          `Lista zadań do zrobienia obejmuje: ${tasks.length} pozycji. Do zrobienia masz: `
        );
        if (tasks.length) {
          tasks.forEach((task, index) => {
            if (index % 2) return console.log(task.title.green);
            return console.log(task.title.yellow);
          });
        }
        break;
    }
  };

  module.exports= handleData;