// ZADANIE 1

// http://numbersapi.com/random/year?json

// const fetch = require("node-fetch");

// const year = process.argv[2] || Math.floor(Math.random() * 2020);

// fetch(`http://numbersapi.com/${year}/year?json`)
//   .then(response => {
//     return response.json();
//   })
//   .then(data => console.log(data.text))
//   .catch(error => console.log("Error", err));

// ZADANIE 2

// const arg = process.argv[2];
// let type = "";

// if (arg.indexOf("--year") === 0) {
//   console.log("Szukamy informacji o roku ...");
//   type = "year";
// } else if (arg.indexOf("--math") === 0) {
//   console.log("Szukamy informacji o liczbie ...");
//   typ = "math";
// } else if (arg.indexOf("--trivia") === 0) {
//   console.log("Szukamy informacji o ciekawostce ...");
//   typ = "trivia";
// }

// const equalSign = arg.search("=");
// if (equalSign === -1) console.log("nie wpisałeś liczy");

// const number = arg.slice(equalSign + 1);
// // console.log(number);

// // if (number === "" || isNaN(Number(number))) {
// //   console.log("To nie jest liczba");
// //   process.exit();
// // }

// fetch(`http://numbersapi.com/${number}/${type}?json`)
//   .then(response => {
//     if (response.ok) {
//       response.json();
//     }else{
//         throw new Error("Coś jest nie tak: "+ response.status);
//     }

//   })
//   .then(response => console.log(response.text))
//   .catch(err => console.log("Błąd: ", err));

// ZADANIE 3

const request = require("request");
const fs = require("fs");

const validCodes = ["usd", "eur", "gbp", "chf"];

const code = process.argv[2];

const isValid = validCodes.find(currency => currency === code) ? true : false;
// console.log(isValid);

const url = `http://api.nbp.pl/api/exchangerates/rates/a/${code}/?format=json`;

request(url, { json: true }, (err, res, body) => {
  if (err) {
    return console.log("Błąd: ", err);
  }

  if (res.statusCode !== 200) {
    return console.log("coś poszło nie tak, sprawdź url");
  }

  const message = `Średnia cena ${body.currency} w dniu ${body.rates[0].effectiveDate} wynosi ${body.rates[0].mid} złotych`;

  fs.appendFile("currencies.txt", message + "\n", err => {
    console.log("Dane dodane do pliku");
  });

  console.log(message);
});
